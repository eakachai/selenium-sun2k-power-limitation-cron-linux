Using this sun2k zero export scheduler
===
1. Build docker image for selenium side runner
>\# docker build -t docker-selenium-side-runner:1.0 .

2. Replace \<USERNAME\> with "your own Huawei user account" and \<PASSWORD\> with "your own Huawei password"

3. Start selenium server (chrome standalone version)
>\# docker-compose up -d

4. Add the following line to Linux's crontab (in user that can run docker)
>\# crontab -e
```
# disable limitation (unlimit)
0 1 22 * * /usr/bin/docker run --rm --name sidetest01 --network sun2kpowerlimitationcronlinux_selenium_network -e TEST_NAME=disable -v <PATH-TO-SIDE-DIR>:/sides -v <PATH-TO-OUT-DIR>:/root/out docker-selenium-side-runner:1.0 >> <YOUR-LOG-FILE-IF-NEEDED> 2>&1
# enable limitation (limit)
0 1 15 * * /usr/bin/docker run --rm --name sidetest01 --network sun2kpowerlimitationcronlinux_selenium_network -e TEST_NAME=enable -v <PATH-TO-SIDE-DIR>:/sides -v <PATH-TO-OUT-DIR>:/root/out docker-selenium-side-runner:1.0 >> <YOUR-LOG-FILE-IF-NEEDED> 2>&1
```

<br/>
<br/>  
