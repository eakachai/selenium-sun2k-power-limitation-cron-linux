FROM node:10-alpine

LABEL Author="Nikita Gryzlov <nixel2007@gmail.com>, modified by GoTSaMa<gotjimon@gmail.com>"

RUN npm install -g selenium-side-runner

ADD docker-entrypoint.sh /opt/bin/docker-entrypoint.sh
# ADD docker-entrypoint-limit-disable.sh /opt/bin/docker-entrypoint.sh
# ADD docker-entrypoint-limit-enable.sh /opt/bin/docker-entrypoint.sh
RUN chmod +x /opt/bin/docker-entrypoint.sh

RUN mkdir /sides
# ADD sides/*.side /sides/sun2k-power-limitation.side

WORKDIR /root

VOLUME [ "/sides" ]

CMD "/opt/bin/docker-entrypoint.sh"
# CMD [ "exec", "/usr/local/bin/node", "/usr/local/bin/selenium-side-runner", "-s http://chromedriver:4444/wd/hub", "--output-directory /root/out", "/sides/*.side" ]